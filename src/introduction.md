# Introduction

## Purpose

This document specifies and tracks the status of the requirements for the espresso machine demonstrator hard- and firmware. 


## Intended Audience

This document provides students and other interested parties a set of features that are implemented in hardware and in the Rust demo firmware. There will be only one hardware prototype and no further revisions are planned.


## Product Scope

The espresso machine is a demonstration application of the Bern RTOS project. The emphasis is on showing off real-time features on a real-world problem.


## Acronyms

|   |   |
|---|---|
| RTD | Resistance Temperature Detector |