# Non-functional Requirements

The non-functional requirements specify how the espresso machine is supposed to *be*. These quality attribute apply to all components of the system. They have no definition of done, as non-functional requirements apply for every release.

The priority refers to the importance for the project that the requirement is met. All non-functional requirements should be met, but if two are contradicting the one with higher priority will be preferred.

## Requirements

| ID | Title | User Story | Priority |
|----|-------|------------|----------|
| N001 | Rancilio Silvia | As a manufacturer, I want the control electronics to use a Racilio Silvia espresso machine as base, because its interiors are well documented and spare parts are available. | high | 
| N002 | Stability | As a manufacturer, I want the machine to run stable with less than 1 system fault per 80h operating hours. | high | 
| N003 | Microcontroller | As a student, I want the machine to be compatible with the STM32F7 module form the Bern University of Applied Sciences, so that I can develop an RTOS based application for the machine. | high |
| N004 | Microprocessor | As a student, I want the machine to be compatible with the STM32MP1 module from the Bern University of Applied Sciences, so that I can develop an embedded linux application for the machine. | low | 
| N005 | Switches | As a student, I want the machine to provide easy to use UI elements (e.g. push buttons), to that I can program an application without the use of complex software. | medium |
| N006 | Documentation | As a student, I want access to all hardware and software design documents, so that I can develop my own application for the machine | high |



## Questions

| Question | Answer |
|----------|--------|
| What is a system fault? | In a system fault the machine does not behave as required. The fault can only be resolved with a system reset (power cycle). |

## Out of Scope

- Embedded Linux/Android will not be part of the project, but the hardware will be compatible with a microprocessor module.