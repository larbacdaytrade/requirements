# Functional Requirements

The functional requirements specify what the espresso machine is supposed to *do*.  Each requirement has a user story and a priority. The stories state what the user expects in the end product, that implies hardware capabilities and feature set of the Rust firmware.


## Requirements

| ID | Title | User Story | Priority | Status |
|----|-------|------------|----------|--------|
| F001 | Repeatability | As a coffee drinker, I want to pull espresso shots with repeatable temperature profiles. | high | planned |
| F002 | Temperature | As a coffee drinker, I want to set the target water temperature, so that I can adjust the machine to my coffee beans. | high | planned |
| F003 | Steam | As a coffee drinker, I want to put the machine in steam mode, so that I can steam milk. | medium | planned |
| F004 | Pressure | As a coffee drinker, I want to adjust the brew pressure, so that I can experiment with novel brewing techniques. | low | planned |
| F005 | Brew Information | As a coffee drinker, I want to know water temperature and brew pressure, so that I know if I need to adjust my preparation method. | high | planned |
| F006 | Temperature Accuracy | As a manufacturer, I want to temperature controller to achieve +/- 3°C accuracy (without water flow), so that the machine delivers repeatable results. | medium | planned |
| F007 | User Analytics | As a manufacturer, I want to log machine usage and state to a server, so that I can improve the espresso machine design. | medium | planned |
| F008 | Eco Mode | As a manufacturer, I want the machine to turn off when its not used for more than 20 min. | medium | planned |
| F009 | Debug Information | As a student, I want the machine to provide as much internal information as possible, so that I can debug my application easily. | low | planned |


## Questions

| Question | Answer |
|----------|--------|


## Out of Scope