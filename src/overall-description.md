# Overall Description

## Product Perspective

A thermostat based two-point controller is even today frequently used in espresso machine designs. This controller type lacks accuracy and repeatability when pulling espresso shots. In this demonstration application the aim is first to upgrade an existing espresso machine by replacing its electronics and electric components, and secondly to implement a control and monitor firmware using the Bern RTOS. The application must be easy to understand in order to showcase the real-time capabilities of the RTOS.

The components of the demo application are shown in the figure below.

![FW components](img/machine-sw.svg)
*Espresso machine firmware components*

The aim is to use preexisting libraries for drivers, network stack and graphics to reduce development effort. A library written in C can be used, if it provides a more functionality than its Rust alternative, or if there is no Rust library.

For this demonstration application a Racilio Silvia single-boiler espresso machine will be used. The figure below shows the fluid components after modification providing an overview of the actuators and sensors.

![Fluid path](img/machine-hw.svg)
*Espresso machine fluid diagram (overpressure valve omitted)*

The Rancilio Silvia controls the brewing temperature based on thermostats without any electronics. The following sensors will be added for the electronic controller:

- Temperature sensors
- Pressure sensor
- Flow meter

## Product Functions

The espresso machine provides features to:

- Brew coffee
- Display brewing profile (temperature, pressure, time)
- Log machine state to a server over a local network

## User Personas

- Coffee Drinker: The goal of a coffee drinker is to get a good and easy to make coffee in the morning. They expect simple control elements and feedback of the brew.

- Manufacturer: Companies producing espresso machines want feedback on how their machines are used to improve on future products. 

- Student: Students can use the espresso machine hardware to develop their own firmware. They expect a thorough documentation and enough flexibility to implement their own project ideas.


## Operating Environment

The first iteration of the hardware is based on an STM32F7 microcontroller board from the Bern University of Applied Sciences. The espresso machine will be used solely in the lab and exhibitions.


## User Documentation

The espresso machine documentation consists of:

- [Hardware and firmware documentation](https://kernel.bern-rtos.org/) with details about the hardware structure and use of the Bern RTOS.
- [Hardware design documents]()

## Assumptions and Dependencies

### Sensors

It is assumed that the following sensor are accurately enough for a consistent brew:

- RTD temperature sensor on the outside of the boiler (perhaps a temperature can be placed in the brew chamber)
- Flow meter can detect small steps in water flow and is not affected by jitter created by the vibration pump
- Pressure sensor can be placed at brew head to measure the relevant water pressure

### Bern RTOS

The Bern RTOS must provide all the necessary features to develop this real-time application and should not introduce bugs to the system.

### HAL

This demonstration is focused on the application of Bern RTOS. It is assumed that the Rust HAL already include most of the hardware support used in this project.