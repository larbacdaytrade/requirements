# Changelog

Requirement changes of the espresso machine demonstrator will be tracked in this document. See [Commits in GitLab](https://gitlab.com/bern-rtos/requirements/kernel/-/commits/master) for details.

## v1.0 (2021-10-27)

**New Requirements**

- All requirements

**Modifications**

- None
